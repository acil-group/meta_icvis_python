# Python Meta iCVIs

This project contains a Python implementation of the Meta-iCVI method.

## Usage

The project contains a demo file at `demo.py` using local datasets loaded with `datasets.py`.
You may change the dataset loaded in the demo file to one of those implemented in the `datasets.DATASETS` variable.

> **Note**
>
> You may find this package as a `compat` module in an updated `metaicvi` package at [https://github.com/AP6YC/metaicvi](https://github.com/AP6YC/metaicvi).
